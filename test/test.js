#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');


if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const ADMIN_USERNAME='admin';
    const ADMIN_PASSWORD='changeme';
    const LOCATION = process.env.LOCATION || 'test';
    const BOOKMARK_NAME = 'test bookmark ' + Math.floor((Math.random() * 100) + 1);
    const BOOKMARK_URL = 'https://cloudron.io';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/login/');
        await waitForElement(By.id('id_username'));
        await browser.findElement(By.id('id_username')).sendKeys(username);
        await browser.findElement(By.id('id_password')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@value="Login"]')).click();
        await browser.sleep(3000);

        await waitForElement(By.xpath('//a[@href="/bookmarks/new"]'));
    }

    async function loginOIDC(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(4000);

        await waitForElement(By.xpath('//a[@href="/oidc/authenticate/"]'));
        await browser.findElement(By.xpath('//a[@href="/oidc/authenticate/"]')).click();

        if (!athenticated_by_oidc) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();

            athenticated_by_oidc = true;
        }

        await waitForElement(By.xpath('//a[@href="/bookmarks/new"]'));
    }

    async function logout140() {
        await browser.get('https://' + app.fqdn + '/logout/');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//input[@value="Login"]'));
    }

    async function logout() {
        if (await browser.findElements(By.xpath('//form[@action="/logout/"]')).then(found => !!found.length)) {
            await browser.get('https://' + app.fqdn);
            await browser.sleep(3000);
            await browser.findElement(By.xpath('//button[text()="Logout"]')).click();
            await browser.sleep(3000);
            await waitForElement(By.xpath('//input[@value="Login"]'));
        } else {
            await logout140();
        }
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn + '/bookmarks');
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//div[@class="title"]/a[contains(., "${BOOKMARK_NAME}")]`));
    }

    async function createBookmark() {
        await browser.get('https://' + app.fqdn + '/bookmarks/new');
        await browser.sleep(2000);
        await browser.findElement(By.id('id_url')).sendKeys(`${BOOKMARK_URL}`);
        await browser.sleep(1000);
        await browser.findElement(By.id('id_title')).sendKeys(`${BOOKMARK_NAME}`);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@value="Save"]')).click();
        await browser.sleep(3000);
        await browser.get('https://' + app.fqdn + '/bookmarks');
        await waitForElement(By.xpath(`//div[@class="title"]/a[contains(., "${BOOKMARK_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('can create project', createBookmark);
    it('check bookmark', contentExists);
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('can create project', createBookmark);
    it('check bookmark', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('check bookmark', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('check bookmark', contentExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('check bookmark', contentExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id link.linkding.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    // replace with loginOIDC on the next release
    it('can login with username', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can create project', createBookmark);
    it('check bookmark', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    // replace with loginOIDC on the next release
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check bookmark', contentExists);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
