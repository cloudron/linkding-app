FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/code/browsers
WORKDIR /app/code

# renovate: datasource=github-releases depName=sissbruecker/linkding versioning=semver extractVersion=^v(?<version>.+)$
ARG LINKDING_VERSION=1.39.1

RUN curl -L https://github.com/sissbruecker/linkding/archive/refs/tags/v${LINKDING_VERSION}.tar.gz | tar -zxvf - --strip-components 1 -C /app/code

# install python3.11 (https://github.com/sissbruecker/linkding/blob/master/docker/alpine.Dockerfile#L13)
RUN apt-get update && \
    add-apt-repository -y ppa:deadsnakes && \
    apt-get install -y --no-install-recommends python3.11 python3.11-venv python3.11-dev && \
    rm -rf /usr/bin/python3 && ln -s /usr/bin/python3.11 /usr/bin/python3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# this is need for the cronjob
RUN ln -s /usr/bin/python3 /usr/bin/python

RUN mkdir /app/code/data && \
    ln -sf /app/data/favicons /app/code/data/favicons && \
    ln -sf /app/data/secretkey.txt /app/code/data/secretkey.txt && \
    ln -sf /app/data/data/assets /app/code/data/assets && \
    ln -sf /app/data/data/previews /app/code/data/previews

#RUN sed -i '/playwright/d' requirements.dev.txt
RUN pip3 install -U pip && \
    pip3 install -r requirements.txt -r requirements.dev.txt && \
    pip3 install supervisor==4.2.1

ENV PLAYWRIGHT_BROWSERS_PATH=/app/code/browsers
RUN playwright install --with-deps chromium

RUN npm install && \
    npm run build && \
    npm cache clean --force

RUN python3 manage.py collectstatic

# the sqlite file is for huey task queue storage
RUN mv /app/code/data/tasks.sqlite3 /app/code/data/tasks.sqlite3.orig && \
    ln -sf /app/data/data/tasks.sqlite3 /app/code/data/tasks.sqlite3

RUN pip3 install uWSGI

# install UBlock
# https://github.com/sissbruecker/linkding/blob/v1.29.0/docker/default.Dockerfile#L106
RUN TAG=$(curl -sL https://api.github.com/repos/gorhill/uBlock/releases/latest | jq -r '.tag_name') && \
    DOWNLOAD_URL=https://github.com/gorhill/uBlock/releases/download/$TAG/uBlock0_$TAG.chromium.zip && \
    curl -L -o uBlock0.zip $DOWNLOAD_URL && \
    unzip uBlock0.zip
# Patch assets.json to enable easylist-cookies by default
RUN curl -L -o ./uBlock0.chromium/assets/thirdparties/easylist/easylist-cookies.txt https://ublockorigin.github.io/uAssets/thirdparties/easylist-cookies.txt
RUN jq '."assets.json" |= del(.cdnURLs) | ."assets.json".contentURL = ["assets/assets.json"] | ."fanboy-cookiemonster" |= del(.off) | ."fanboy-cookiemonster".contentURL += ["assets/thirdparties/easylist/easylist-cookies.txt"]' ./uBlock0.chromium/assets/assets.json > temp.json && \
    mv temp.json ./uBlock0.chromium/assets/assets.json

# install node v20 (https://github.com/sissbruecker/linkding/blob/master/docker/default.Dockerfile#L1)
ARG NODE_VERSION=20.11.1

RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH="/usr/local/node-${NODE_VERSION}/bin:$PATH"
RUN corepack enable

# install single-file from fork for now, which contains several hotfixes
# https://github.com/sissbruecker/linkding/blob/master/docker/default.Dockerfile#L1
RUN npm install -g https://github.com/sissbruecker/single-file-cli/tarball/4c54b3bc704cfb3e96cec2d24854caca3df0b3b6

RUN ln -sf $(python3 -c 'from playwright.sync_api import sync_playwright; print(sync_playwright().start().chromium.executable_path)')  /usr/bin/chromium

ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY uwsgi.ini start.sh /app/pkg/

RUN chown -R cloudron:cloudron /app/code

# Run bootstrap logic
CMD ["/app/pkg/start.sh"]
