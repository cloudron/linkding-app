### About

Linkding is a bookmark manager that is designed be to be minimal, fast, and easy to host yourself.

The name comes from:

* link which is often used as a synonym for URLs and bookmarks in common language
* Ding which is German for thing
* ...so basically something for managing your links

### Feature Overview:

* Clean UI optimized for readability
* Organize bookmarks with tags
* Add notes using Markdown
* Read it later functionality
* Share bookmarks with other users
* Bulk editing
* Automatically provides titles, descriptions and icons of bookmarked websites
* Automatically creates snapshots of bookmarked websites on the Internet Archive Wayback Machine
* Import and export bookmarks in Netscape HTML format
* Extensions for Firefox and Chrome, as well as a bookmarklet
* Light and dark themes
* REST API for developing 3rd party apps
* Admin panel for user self-service and raw data access
* Easy setup using Docker and a SQLite database, with PostgreSQL as an option

