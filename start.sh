#!/bin/bash

set -eu

echo "=> Starting Linkding"

echo "=> Creating directories"
mkdir -p /app/data/favicons /app/data/data/assets /app/data/data/previews /run/linkding/chromium-profile

if [[ ! -f /app/data/data/tasks.sqlite3 ]]; then
    cp /app/code/data/tasks.sqlite3.orig /app/data/data/tasks.sqlite3
fi

if [[ ! -f /app/data/secretkey.txt ]]; then
    echo "==> Generate secret key"
    python3 manage.py generate_secret_key
fi


# https://github.com/sissbruecker/linkding/blob/master/docs/Options.md
echo "==> Update database settings"
cat > /run/linkding/env.sh <<EOT
export LD_SERVER_PORT=9090
export LD_DB_ENGINE="postgres"
export LD_DB_DATABASE="${CLOUDRON_POSTGRESQL_DATABASE}"
export LD_DB_USER="${CLOUDRON_POSTGRESQL_USERNAME}"
export LD_DB_PASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}"
export LD_DB_HOST="${CLOUDRON_POSTGRESQL_HOST}"
export LD_DB_PORT="${CLOUDRON_POSTGRESQL_PORT}"

export LD_LOG_X_FORWARDED_FOR=true

export LD_SINGLEFILE_UBLOCK_OPTIONS='--browser-arg="--user-data-dir=/run/linkding/chromium-profile"'

EOT

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then

    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    cat >> /run/linkding/env.sh <<EOT
export LD_ENABLE_OIDC=True
export OIDC_RP_SIGN_ALGO=RS256
export OIDC_OP_JWKS_ENDPOINT="${CLOUDRON_OIDC_ISSUER}/jwks"
export OIDC_OP_AUTHORIZATION_ENDPOINT="${CLOUDRON_OIDC_ISSUER}/auth"
export OIDC_OP_TOKEN_ENDPOINT="${CLOUDRON_OIDC_ISSUER}/token"
export OIDC_OP_USER_ENDPOINT="${CLOUDRON_OIDC_ISSUER}/me"
export OIDC_RP_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
export OIDC_RP_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
EOT
fi

if [[ ! -f /app/data/env.sh ]]; then
    echo -e "# Add custom env vars here (https://github.com/sissbruecker/linkding/blob/master/docs/Options.md)\n\n#export LD_DISABLE_URL_VALIDATION=True\n\n# enable snapshot support\nexport LD_ENABLE_SNAPSHOTS=True" > /app/data/env.sh
fi
cat /app/data/env.sh >> /run/linkding/env.sh
source /run/linkding/env.sh

echo "=> Run database migration"
python3 manage.py migrate

if [[ ! -f /app/data/.initialized ]]; then
    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@cloudron.local', 'changeme')" | python3 manage.py shell
    touch /app/data/.initialized
fi

echo "=> Changing permissions"
chown -R cloudron:cloudron /app/data /run/linkding

echo "=> Starting Linkding"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Linkding
