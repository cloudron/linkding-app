[0.1.0]
* Initial version for Linkding

[0.2.0]
* Some package cleanups

[0.3.0]
* Bump memory limit to 512MB

[1.0.0]
* Initial stable release
* customizable env.sh

[1.1.0]
* Update Linkding to 1.20.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.20.0)
* Add option to share bookmarks publicly by @sissbruecker in #503
* Various improvements to favicons by @sissbruecker in #504
* Add support for PRIVATE flag in import and export by @sissbruecker in #505
* Avoid page reload when triggering actions in bookmark list by @sissbruecker in #506

[1.1.1]
* Update Linkding to 1.20.1
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.20.1)
* Update cached styles and scripts after version change by @sissbruecker in https://github.com/sissbruecker/linkding/pull/510

[1.2.0]
* Update Linkding to 1.21.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.21.0)

[1.2.1]
* Update Linkding to 1.21.1
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.21.1)
* Fix bulk edit to respect searched tags by @sissbruecker

[1.3.0]
* Update Linkding to 1.22.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.22.0)
* Fix case-insensitive search for unicode characters in SQLite by @sissbruecker in #520
* Add sort option to bookmark list by @sissbruecker in #522
* Add button to show tags on smaller screens by @sissbruecker in #529
* Make code blocks in notes scrollable by @sissbruecker in #530
* Add filter for shared state by @sissbruecker in #531
* Add support for exporting/importing bookmark notes by @sissbruecker in #532

[1.3.1]
* Update Linkding to 1.22.1
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.22.1)

[1.3.2]
* Update Linkding to 1.22.2
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.22.2)
* Fix search options not opening on iOS by @sissbruecker in #549
* Bump urllib3 from 1.26.11 to 1.26.17 by @dependabot in #542
* Add iOS shortcut to community section by @andrewdolphin in #550
* Disable editing of search preferences in user admin by @sissbruecker in #555
* Add feed2linkding to community section by @Strubbl in #544

[1.3.3]
* Update Linkding to 1.22.3
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.22.3)
* Fix RSS feed not handling None values by @vitormarcal in #569

[1.3.4]
* Update Linkding to 1.23.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.23.0)
* Add backup CLI command by @sissbruecker in https://github.com/sissbruecker/linkding/pull/571
* Update browser extension links by @OPerepadia in https://github.com/sissbruecker/linkding/pull/574
* Include archived bookmarks in export by @sissbruecker in https://github.com/sissbruecker/linkding/pull/579

[1.3.5]
* Update Linkding to 1.23.1
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.23.1)
* This resolves a security vulnerability in linkding. Everyone is encouraged to upgrade to the latest version as soon as possible

[1.4.0]
* Update Linkding to 1.24.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.24.0)
* Support Open Graph description by @jonathan-s in #602
* Add tooltip to truncated bookmark titles by @jonathan-s in #607
* Improve bulk tag performance by @sissbruecker in #612
* Increase tag limit in tag autocomplete by @hypebeast in #581
* Bump playwright dependencies by @jonathan-s in #601
* Adjust archive.org donation link in general.html by @JnsDornbusch in #603

[1.5.0]
* Update Linkding to 1.25.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.25.0)
* Improve PWA capabilities by @hugo-vrijswijk in #630
* build improvements by @hugo-vrijswijk in #649
* Add support for oidc by @Nighmared in #389
* Add option for custom CSS by @sissbruecker in #652
* Update backup location to safe directory by @bphenriques in #653
* Include web archive link in /api/bookmarks/ by @sissbruecker in #655
* Add RSS feeds for shared bookmarks by @sissbruecker in #656
* Bump django from 5.0.2 to 5.0.3 by @dependabot in #658

[1.6.0]
* This release enables optional OpenID integration

[1.7.0]
* Update Linkding to 1.26.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.26.0)
* Add option for showing bookmark description as separate block by @sissbruecker in #663
* Add bookmark details view by @sissbruecker in #665
* Make bookmark list actions configurable by @sissbruecker in #666
* Bump black from 24.1.1 to 24.3.0 by @dependabot in #662

[1.8.0]
* Update Linkding to 1.27.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.27.0)
* Archive snapshots of websites locally by @sissbruecker in #672
* Add Railway hosting option by @tianheg in #661
* Add how to for increasing the font size by @sissbruecker in #667

[1.8.1]
* Update Linkding to 1.27.1
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.27.1)
* Fix HTML snapshot errors related to single-file-cli by @sissbruecker in https://github.com/sissbruecker/linkding/pull/683
* Replace django-background-tasks with huey by @sissbruecker in https://github.com/sissbruecker/linkding/pull/657
* Add Authelia OIDC example to docs by @hugo-vrijswijk in https://github.com/sissbruecker/linkding/pull/675

[1.9.0]
* Update Linkding to 1.28.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.28.0)
* Add option to disable SSL verification for OIDC by @akaSyntaax in #684
* Add full backup method by @sissbruecker in #686
* Truncate snapshot filename for long URLs by @sissbruecker in #687
* Add option for customizing single-file timeout by @pettijohn in #688
* Add option for passing arguments to single-file command by @pettijohn in #691
* Fix typo by @tianheg in #689

[1.10.0]
* Update Linkding to 1.29.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.29.0)
* Remove ads and cookie banners from HTML snapshots by @sissbruecker in #695
* Add button for creating missing HTML snapshots by @sissbruecker in #696
* Refresh file list when there are queued snapshots by @sissbruecker in #697
* Bump idna from 3.6 to 3.7 by @dependabot in #694

[1.10.1]
* Fix webpage snapshotting via chromium/playwright

[1.11.0]
* Update Linkding to 1.30.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.30.0)
* Add reader mode by @sissbruecker in https://github.com/sissbruecker/linkding/pull/703
* Allow uploading custom files for bookmarks by @sissbruecker in https://github.com/sissbruecker/linkding/pull/713
* Add option for marking bookmarks as unread by default by @ab623 in https://github.com/sissbruecker/linkding/pull/706
* Make blocking cookie banners more reliable by @sissbruecker in https://github.com/sissbruecker/linkding/pull/699
* Close bookmark details with escape by @sissbruecker in https://github.com/sissbruecker/linkding/pull/702
* Show proper name for bookmark assets in admin by @ab623 in https://github.com/sissbruecker/linkding/pull/708
* Bump sqlparse from 0.4.4 to 0.5.0 by @dependabot in https://github.com/sissbruecker/linkding/pull/704

[1.12.0]
* Update Linkding to 1.31.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.31.0)
* Add support for bookmark thumbnails by @vslinko in #721
* Automatically add tags to bookmarks based on URL pattern by @vslinko in #736
* Load bookmark thumbnails after import by @vslinko in #724
* Load missing thumbnails after enabling the feature by @sissbruecker in #725
* Thumbnails lazy loading by @vslinko in #734

[1.12.1]
* Update nodejs to make snapshotting work

[1.12.2]
* Update Linkding to 1.31.2
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.31.2)

[1.13.0]
* Update Linkding to 1.32.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.32.0)
* Allow configuring landing page for unauthenticated users by @sissbruecker in https://github.com/sissbruecker/linkding/pull/808
* Allow configuring guest user profile by @sissbruecker in https://github.com/sissbruecker/linkding/pull/809
* Return bookmark tags in RSS feeds by @sissbruecker in https://github.com/sissbruecker/linkding/pull/810
* Additional filter parameters for RSS feeds by @sissbruecker in https://github.com/sissbruecker/linkding/pull/811
* Allow pre-filling notes in new bookmark form by @sissbruecker in https://github.com/sissbruecker/linkding/pull/812
* Fix inconsistent tag order in bookmarks by @sissbruecker in https://github.com/sissbruecker/linkding/pull/819
* Fix auto-tagging when URL includes port by @sissbruecker in https://github.com/sissbruecker/linkding/pull/820

[1.14.0]
* Update Linkding to 1.33.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.33.0)
* Theme improvements by @sissbruecker in https://github.com/sissbruecker/linkding/pull/822
* Speed up navigation by @sissbruecker in https://github.com/sissbruecker/linkding/pull/824
* Rename "SingeFileError" to "SingleFileError" by @curiousleo in https://github.com/sissbruecker/linkding/pull/823
* Bump svelte from 4.2.12 to 4.2.19 by @dependabot in https://github.com/sissbruecker/linkding/pull/806

[1.14.1]
* Update Linkding to 1.34.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.34.0)
* Fix several issues around browser back navigation by @sissbruecker in https://github.com/sissbruecker/linkding/pull/825
* Speed up response times for certain actions by @sissbruecker in https://github.com/sissbruecker/linkding/pull/829
* Implement IPv6 capability by @itz-Jana in https://github.com/sissbruecker/linkding/pull/826

[1.15.0]
* Update Linkding to 1.35.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.35.0)
* Add configuration options for pagination by @sissbruecker in #835
* Show placeholder if there is no preview image by @sissbruecker in #842
* Allow bookmarks to have empty title and description by @sissbruecker in #843
* Add clear buttons in bookmark form by @sissbruecker in #846
* Add basic fail2ban support by @sissbruecker in #847
* Add documentation website by @sissbruecker in #833
* Add go-linkding to community projects by @piero-vic in #836
* Fix a broken link to options documentation by @zbrox in #844

[1.16.0]
* Update Linkding to 1.36.0
* [Full changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.36.0)
* Replace uBlock Origin with uBlock Origin Lite by @sissbruecker in #866
* Add `LAST_MODIFIED` attribute when exporting by @ixzhao in #860
* Return client error status code for invalid form submissions by @sissbruecker in #849
* Fix header.svg text by @vladh in #850
* Do not clear fields in POST requests (API behavior change) by @sissbruecker in #852
* Prevent duplicates when editing by @sissbruecker in #853
* Fix jumping details modal on back navigation by @sissbruecker in #854
* Fix select dropdown menu background in dark theme by @sissbruecker in #858
* Do not escape valid characters in custom CSS by @sissbruecker in #863
[1.16.1]
* checklist added to CloudronManifest

[1.17.0]
* Update linkding to 1.37.0
* [Full Changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.37.0)
* Add option to disable request logs by [@&#8203;dmarcoux](https://github.com/dmarcoux) in https://github.com/sissbruecker/linkding/pull/887
* Add default robots.txt to block crawlers by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/959
* Fix menu dropdown focus traps by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/944
* Provide accessible name to radio groups by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/945
* Add serchding to community projects, sort the list by alphabetical order by [@&#8203;ldwgchen](https://github.com/ldwgchen) in https://github.com/sissbruecker/linkding/pull/880
* Add cosmicding To Community Resources by [@&#8203;vkhitrin](https://github.com/vkhitrin) in https://github.com/sissbruecker/linkding/pull/892
* Add 3 new community projects by [@&#8203;sebw](https://github.com/sebw) in https://github.com/sissbruecker/linkding/pull/949
* Add a rust client library to community.md by [@&#8203;zbrox](https://github.com/zbrox) in https://github.com/sissbruecker/linkding/pull/914
* Update community.md by [@&#8203;justusthane](https://github.com/justusthane) in https://github.com/sissbruecker/linkding/pull/897
* Bump astro from 4.15.8 to 4.16.3 in /docs by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/884
* Bump vite from 5.4.9 to 5.4.14 in /docs by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/953
* Bump django from 5.1.1 to 5.1.5 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/947
* Bump nanoid from 3.3.7 to 3.3.8 by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/928
* Bump astro from 4.16.3 to 4.16.18 in /docs by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/929
* Bump nanoid from 3.3.7 to 3.3.8 in /docs by [@&#8203;dependabot](https://github.com/dependabot) in https://github.com/sissbruecker/linkding/pull/962
* [@&#8203;ldwgchen](https://github.com/ldwgchen) made their first contribution in https://github.com/sissbruecker/linkding/pull/880
* [@&#8203;dmarcoux](https://github.com/dmarcoux) made their first contribution in https://github.com/sissbruecker/linkding/pull/887
* [@&#8203;vkhitrin](https://github.com/vkhitrin) made their first contribution in https://github.com/sissbruecker/linkding/pull/892
* [@&#8203;sebw](https://github.com/sebw) made their first contribution in https://github.com/sissbruecker/linkding/pull/949
* [@&#8203;justusthane](https://github.com/justusthane) made their first contribution in https://github.com/sissbruecker/linkding/pull/897

[1.18.0]
* Update linkding to 1.38.0
* [Full Changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.38.0)
* Fix nav menu closing on mousedown in Safari by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/965
* Allow customizing username when creating user through OIDC by [@&#8203;kyuuk](https://github.com/kyuuk) in https://github.com/sissbruecker/linkding/pull/971
* Improve accessibility of modal dialogs by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/974
* Add option to collapse side panel by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/975
* Convert tag modal into drawer by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/977
* Add RSS link to shared bookmarks page by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/984
* Add Additional iOS Shortcut to community section by [@&#8203;joshdick](https://github.com/joshdick) in https://github.com/sissbruecker/linkding/pull/968
* [@&#8203;kyuuk](https://github.com/kyuuk) made their first contribution in https://github.com/sissbruecker/linkding/pull/971

[1.18.1]
* Update linkding to 1.38.1
* [Full Changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.38.1)
* Remove preview image when bookmark is deleted by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/989
* Try limit uwsgi memory usage by configuring file descriptor limit by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/990
* Add note about OIDC and LD_SUPERUSER_NAME combination by [@&#8203;tebriel](https://github.com/tebriel) in https://github.com/sissbruecker/linkding/pull/992
* Return web archive fallback URL from REST API by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/993
* Fix auth proxy logout by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/994
* [@&#8203;tebriel](https://github.com/tebriel) made their first contribution in https://github.com/sissbruecker/linkding/pull/992

[1.19.0]
* Update linkding to 1.39.1
* [Full Changelog](https://github.com/sissbruecker/linkding/releases/tag/v1.39.1)
* Add REST endpoint for uploading snapshots from the Singlefile extension by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/996
* Add bookmark assets API by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/1003
* Allow providing REST API authentication token with Bearer keyword by [@&#8203;sissbruecker](https://github.com/sissbruecker) in https://github.com/sissbruecker/linkding/pull/995
* Add Telegram bot to community section by [@&#8203;marb08](https://github.com/marb08) in https://github.com/sissbruecker/linkding/pull/1001
* Adding linklater to community projects by [@&#8203;nsartor](https://github.com/nsartor) in https://github.com/sissbruecker/linkding/pull/1002
* [@&#8203;marb08](https://github.com/marb08) made their first contribution in https://github.com/sissbruecker/linkding/pull/1001
* [@&#8203;nsartor](https://github.com/nsartor) made their first contribution in https://github.com/sissbruecker/linkding/pull/1002

